# Sample Messenger

## プライバシーポリシー

### 利用者情報
本アプリは利用者の性別と位置情報を取得します。

### 利用者情報の利用
本アプリは利用者の性別と位置情報をメッセージの作成に用います。

### 利用者情報の第三者提供
本アプリが利用者の個人情報を第三者へ提供することはありません。

### お問い合わせ先
何かご不明な点がございましたらお問い合わせください。

nagata.minoru8823@gmail.com